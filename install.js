const fs = require('fs');
const child_process = require('child_process');

function config() {
    const directory_from = `${__dirname}/template/config`;
    const directory_to = `${__dirname}/../src/config`;

    fs.copyFileSync(`${directory_from}/config.json`, `${directory_to}/config.json`);
    fs.copyFileSync(`${directory_from}/layouts.json`, `${directory_to}/layouts.json`);
    fs.copyFileSync(`${directory_from}/ru-RU.json`, `${directory_to}/ru-RU.json`);
    fs.copyFileSync(`${directory_from}/widgets.json`, `${directory_to}/widgets.json`);
    fs.copyFileSync(`${directory_from}/views.json`, `${directory_to}/views.json`);
    fs.writeFileSync(`${directory_to}/.gitignore`, 'config.json', 'utf8');
}

function clone(url, directory, branch) {
    directory = `${__dirname}/../src/${directory}`;
    if (!branch) branch = 'master';
    if (!fs.existsSync(directory)) {
        child_process.execSync(`git clone ${url} -b ${branch} "${directory}"`);
    }
}

function cloneWidget(url, directory) {
    directory = `${__dirname}/../src/widgets/${directory}`;
    if (!fs.existsSync(directory)) {
        child_process.execSync(`git clone ${url} "${directory}"`);
    }
}

function cloneView(url, directory, branch) {
    directory = `${__dirname}/../src/states/${directory}`;
    if (!branch) branch = 'master';
    if (!fs.existsSync(directory)) {
        child_process.execSync(`git clone ${url} -b ${branch} "${directory}"`);
    }
}

function installViewPackages(directory) {
    const packageFile = `${__dirname}/../src/states/${directory}/package.json`;
    if (fs.existsSync(packageFile)) {
        const packageObject = require(packageFile);
        for (const name in packageObject.dependencies) {
            const version = packageObject.dependencies[name];
            child_process.execSync(`npm i --no-save ${name}@${version}`);
        }
    }
}

if (!fs.existsSync(`${__dirname}/../src`)) {
    fs.mkdirSync(`${__dirname}/../src`);
}

if (!fs.existsSync(`${__dirname}/../src/index.js`)) {
    const directory_from = `${__dirname}/template`;
    const directory_to = `${__dirname}/../src`;

    fs.copyFileSync(`${directory_from}/index.js`, `${directory_to}/index.js`);
}

if (!fs.existsSync(`${__dirname}/../src/config`)) {
    fs.mkdirSync(`${__dirname}/../src/config`);
    config();
}

if (!fs.existsSync(`${__dirname}/../src/widgets`)) {
    fs.mkdirSync(`${__dirname}/../src/widgets`);
}

if (!fs.existsSync(`${__dirname}/../src/states`)) {
    fs.mkdirSync(`${__dirname}/../src/states`);
}

clone('https://gitlab.com/protopiahome-public/protopia-ecosystem/react-client/libraries/layouts.git', 'layouts', 'master');
clone('https://gitlab.com/protopiahome-public/protopia-ecosystem/react-client/libraries/layoutapp.git', 'LayoutApp', 'master');

const widgets = require('../src/config/widgets.json');
const views = require('../src/config/views.json');

if (widgets) {
    widgets.forEach((e, i) => {
        if (e.url && e.name) {
            cloneWidget(e.url, e.name);
        }
    });
}

if (views) {
    views.forEach((e, i) => {
        if (e.url && e.name) {
            cloneView(e.url, e.name);
            installViewPackages(e.name);
        }
    });
}
